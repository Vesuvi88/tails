[[!meta title="Monthly reports"]]

[[!map pages="blueprint/monthly_report/*" show="title"]]

This page could be a good place to gather HOWTO, tips, template, etc.
for the monthly reports.

- Check the archives of:
  - <https://mailman.boum.org/pipermail/tails-dev/>
  - <https://mailman.boum.org/pipermail/tails-ux/>
  - <https://mailman.boum.org/pipermail/tails-project/>
- Explore the Git history:
  - git log --since="January 1" --until="February 1"
- Redmine view of closed issues:
  - https://labs.riseup.net/code/projects/tails/issues?utf8=%E2%9C%93&set_filter=1&f[]=closed_on&op[closed_on]=%3E%3C&v[closed_on][]=2016-01-01&v[closed_on][]=2016-01-31&f[]=&c[]=tracker&c[]=status&c[]=priority&c[]=subject&c[]=author&c[]=assigned_to&c[]=updated_on&c[]=cf_9&group_by=
