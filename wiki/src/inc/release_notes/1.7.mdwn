## New features

- You can now start Tails in [[offline mode|doc/first_steps/startup_options/offline_mode]]
  to disable all networking for additional security. Doing so can be
  useful when working on sensitive documents.

- We added <span class="application">[[Icedove|doc/anonymous_internet/icedove]]</span>,
  a rebranded version of the <span class="application">Mozilla
  Thunderbird</span> email client.

  <span class="application">Icedove</span> is currently a technology preview. It is safe to use in the
  context of Tails but it will be better integrated in future versions
  until we remove <span class="application">[[Claws
  Mail|doc/anonymous_internet/claws_mail]]</span>. Users of <span class="application">Claws
  Mail</span> should refer to our instructions to [[migrate their data from
  <span class="application">Claws Mail</span> to
  <span class="application">Icedove</span>|doc/anonymous_internet/claws_mail_to_icedove]].

## Upgrades and changes

- Improve the wording of the first screen of
  <span class="application">Tails Installer</span>.

- Restart Tor automatically if connecting to the Tor network takes too
  long. ([[!tails_ticket 9516 desc="#9516"]])

- Update several firmware packages which might improve hardware
  compatibility.

- Update the Tails signing key which is now valid until 2017.

- Update <span class="application">Tor Browser</span> to 5.0.4.

- Update Tor to 0.2.7.4.

## Fixed problems

- Prevent <span class="command">wget</span> from leaking the IP address
  when using the FTP protocol. ([[!tails_ticket 10364 desc="#10364"]])

- Prevent symlink attack on <span class="filename">~/.xsession-errors</span>
  via <span class="command">tails-debugging-info</span> which could be
  used by the amnesia user to bypass read permissions on any file.
  ([[!tails_ticket 10333 desc="#10333"]])

- Force synchronization of data on the USB stick at the end of automatic
  upgrades. This might fix some reliability bugs in automatic upgrades.

- Make the "I2P is ready" notification more reliable.
