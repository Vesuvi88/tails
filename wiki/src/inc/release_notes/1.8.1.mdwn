## Upgrades and changes

- Tor Browser to [5.0.6](https://gitweb.torproject.org/builders/tor-browser-bundle.git/tree/Bundle-Data/Docs/ChangeLog.txt?h=maint-5.0&id=tbb-5.0.6-build1)

## Fixed problems

- Fix time synchronization in bridge mode.
