# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2016-02-01 19:35+0100\n"
"PO-Revision-Date: 2015-12-31 13:49+0100\n"
"Last-Translator: Tails translators <tails@boum.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.6.10\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Make a donation\"]]\n"
msgstr "[[!meta title=\"Eine Spende tätigen\"]]\n"

#. type: Plain text
#, no-wrap
msgid ""
"Tails is free because <strong>nobody should have to pay to be safe\n"
"while using computers</strong>. But Tails cannot stay alive without\n"
"money and <strong>we need your help</strong>!\n"
msgstr ""
"Tails ist kostenlos, da <strong>niemand für die sichere Nutzung eines Computers\n"
"bezahlen müssen sollte</strong>. Allerdings kann Tails nicht ohne finanzielle\n"
"Unterstützung bestehen, daher <strong>brauchen wir Ihre Hilfe</strong>!\n"

#. type: Plain text
#, no-wrap
msgid ""
"<strong>[[Discover who you are helping around the world when\n"
"donating to Tails.|news/who_are_you_helping]]</strong>\n"
msgstr ""
"<strong>[[Erfahren Sie mehr, wem Sie weltweit mit einer Spende an Tails\n"
"helfen.|news/who_are_you_helping]]</strong>\n"

#. type: Plain text
msgid ""
"Note that Tails is a project mainly run by volunteers. There are [[many "
"other ways to contribute|contribute]]!"
msgstr ""
"Beachten Sie, dass Tails ein hauptsächlich von Freiwilligen betriebenes "
"Projekt ist. Es gibt [[viele andere Wege sich einzubringen|contribute]]!"

#. type: Title =
#, no-wrap
msgid "Ways to donate\n"
msgstr "Möglichkeiten zu Spenden\n"

#. type: Bullet: '  * '
msgid ""
"Crowdfunding campaign run by the American organization [Freedom of the Press "
"Foundation](https://pressfreedomfoundation.org/bundle/encryption-tools-"
"journalists)."
msgstr ""
"Crowdfunding Kampagne der amerikanischen Organisation [Freedom of the Press "
"Foundation](https://pressfreedomfoundation.org/bundle/encryption-tools-"
"journalists)."

#. type: Plain text
#, no-wrap
msgid "    If you live in the US, your donation will be tax-deductible.\n"
msgstr "    Falls Sie in den USA leben wird Ihre Spende von der Steuer absetzbar sein.\n"

#. type: Bullet: '  * '
msgid ""
"[[Bank wire transfer|donate#swift]] or [[Paypal|donate#paypal]] through the "
"German organization [Zwiebelfreunde e.V.](https://www.zwiebelfreunde.de/)."
msgstr ""
"[[Banküberweisung|donate#swift]] oder [[Paypal|donate#paypal]] über die "
"deutsche Organisation [Zwiebelfreunde e.V.](https://www.zwiebelfreunde.de/)."

#. type: Plain text
#, no-wrap
msgid ""
"    If you live in Europe, your donation might be tax-deductible. Check what are\n"
"    the precise conditions in your country, and [ask\n"
"    Zwiebelfreunde](https://www.torservers.net/contact.html) for a donation\n"
"    receipt if you need one.\n"
msgstr ""
"    Wenn Sie in Europa leben, lässt sich Ihre Spende möglicherweise von der Steuer absetzen. Überprüfen Sie die genauen\n"
"    Bedingungen in Ihrem Land und [fragen Sie \n"
"    Zwiebelfreunde](https://www.torservers.net/contact.html) nach einer\n"
"    Spendenquittung, falls Sie eine benötigen.\n"

#. type: Bullet: '  * '
msgid "[[Bitcoin|donate#bitcoin]]"
msgstr "[[Bitcoin|donate#bitcoin]]"

#. type: Bullet: '  * '
msgid ""
"[Flattr](https://flattr.com/submit/auto?user_id=tails_live&url=https://tails."
"boum.org&title=Tails)"
msgstr ""
"[Flattr](https://flattr.com/submit/auto?user_id=tails_live&url=https://tails."
"boum.org&title=Tails)"

#. type: Bullet: '  * '
msgid ""
"For fundraising related matters, you can write to <tails-fundraising@boum."
"org>."
msgstr ""

#. type: Bullet: '  * '
msgid ""
"If none of these methods suit you, consider [donating to the Tor Project]"
"(https://www.torproject.org/donate/). They do great work, and also support "
"us financially."
msgstr ""
"Falls Ihnen keine dieser Methoden zusagt, können Sie auch [eine Spende an "
"das Tor Project](https://www.torproject.org/donate/) in Betracht ziehen. Sie "
"leisten großartige Arbeit und unterstützen uns ebenfalls finanziell."

#. type: Plain text
msgid "Thank you for your donation!"
msgstr "Vielen Dank für Ihre Spende!"

#. type: Plain text
#, no-wrap
msgid "<a id=\"bitcoin\"></a>\n"
msgstr "<a id=\"bitcoin\"></a>\n"

#. type: Title -
#, no-wrap
msgid "Bitcoin\n"
msgstr "Bitcoin\n"

#. type: Plain text
#, no-wrap
msgid "You can send Bitcoins to **<a href=\"bitcoin:1BvBMSEYstWetqTFn5Au4m4GFg7xJaNVN2\">1BvBMSEYstWetqTFn5Au4m4GFg7xJaNVN2</a>**.\n"
msgstr "Sie können Bitcoins an **<a href=\"bitcoin:1BvBMSEYstWetqTFn5Au4m4GFg7xJaNVN2\">1BvBMSEYstWetqTFn5Au4m4GFg7xJaNVN2</a>** senden.\n"

#. type: Plain text
#, no-wrap
msgid "<div class=\"caution\">\n"
msgstr "<div class=\"caution\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>Bitcoin is <a href=\"https://bitcoin.org/en/faq#is-bitcoin-anonymous\">not\n"
"anonymous</a>.</p>\n"
msgstr ""
"<p>Bitcoin ist <a href=\"https://bitcoin.org/de/faq#ist-bitcoin-anonym\">nicht\n"
"anonym</a>.</p>\n"

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"

#. type: Plain text
#, no-wrap
msgid "<a id=\"swift\"></a>\n"
msgstr "<a id=\"swift\"></a>\n"

#. type: Title -
#, no-wrap
msgid "Bank wire transfer\n"
msgstr "Banküberweisung\n"

#. type: Plain text
#, no-wrap
msgid ""
"    Account holder: Zwiebelfreunde e.V.\n"
"    Name of bank: GLS Gemeinschaftsbank eG\n"
"    IBAN: DE25430609671126825603\n"
"    BIC: GENODEM1GLS\n"
"    Address of bank: Christstrasse 9, 44789 Bochum, Germany\n"
msgstr ""
"    Kontoinhaber: Zwiebelfreunde e.V.\n"
"    Name der Bank: GLS Gemeinschaftsbank eG\n"
"    IBAN: DE25430609671126825603\n"
"    BIC: GENODEM1GLS\n"
"    Adresse der Bank: Christstrasse 9, 44789 Bochum, Deutschland\n"

#. type: Plain text
#, no-wrap
msgid "<a id=\"paypal\"></a>\n"
msgstr "<a id=\"paypal\"></a>\n"

#. type: Title -
#, no-wrap
msgid "Paypal\n"
msgstr "Paypal\n"

#. type: Plain text
msgid ""
"Please, use the euro (EUR) as currency as this makes accounting easier. "
"However, Paypal automatically converts it to your local currency."
msgstr ""
"Bitte benutzen Sie Euro (EUR) als Währung, da dies die Buchhaltung "
"vereinfacht. Paypal wird die Umrechnung in Ihre lokale Währung übernehmen."

#. type: Title ###
#, no-wrap
msgid "Set up a recurring donation"
msgstr "Eine Spende per Dauerauftrag einrichten"

#. type: Plain text
#, no-wrap
msgid ""
"<form action=\"https://www.paypal.com/cgi-bin/webscr\" method=\"post\" target='_blank' class='donation'>\n"
"\t<input type=\"hidden\" name=\"cmd\" value=\"_xclick-subscriptions\"/>\n"
"\t<input type=\"hidden\" name=\"business\" value=\"tails@torservers.net\"/>\n"
"\t<input type=\"hidden\" name=\"item_name\" value=\"Tails recurring donation\"/>\n"
"\t<input type=\"hidden\" name=\"no_note\" value=\"1\"/>\n"
"\t<input type=\"hidden\" name=\"src\" value=\"1\"/>\n"
"\t<input type=\"hidden\" name=\"modify\" value=\"1\"/>\n"
"\t<input type=\"hidden\" name=\"t3\" value=\"M\"/>\n"
"\t<input name=\"lc\" type=\"hidden\" value=\"US\" />\n"
"\t<input type=\"radio\" name=\"a3\" value=\"5\" id=\"sub5\" checked=\"checked\" /><label for=\"sub5\">5</label>\n"
"\t<input type=\"radio\" name=\"a3\" value=\"10\" id=\"sub10\"/><label for=\"sub10\">10</label>\n"
"\t<input type=\"radio\" name=\"a3\" value=\"20\" id=\"sub20\"/><label for=\"sub20\">20</label>\n"
"\t<input type=\"radio\" name=\"a3\" value=\"50\" id=\"sub50\"/><label for=\"sub50\">50</label>\n"
"\t<input type=\"radio\" name=\"a3\" value=\"100\" id=\"sub100\"/><label for=\"sub100\">100</label>\n"
"\t<input type=\"radio\" name=\"a3\" value=\"250\" id=\"sub250\"/><label for=\"sub250\">250</label>\n"
"\t<input type=\"radio\" name=\"a3\" value=\"500\" id=\"sub500\"/><label for=\"sub500\">500</label>\n"
"\t<select name=\"currency_code\">\n"
"\t\t\t<option value='EUR'>EUR</option>\n"
"\t\t\t<option value='USD'>USD</option>\n"
"\t\t\t<option value='GBP'>GBP</option>\n"
"\t\t\t<option value='CAD'>CAD</option>\n"
"\t\t\t<option value='AUD'>AUD</option>\n"
"\t\t\t<option value='NZD'>NZD</option>\n"
"\t\t\t<option value='SEK'>SEK</option>\n"
"\t\t\t<option value='CZK'>CZK</option>\n"
"\t\t\t<option value='PLN'>PLN</option>\n"
"\t\t\t<option value='DKK'>DKK</option>\n"
"\t\t\t<option value='NOK'>NOK</option>\n"
"\t\t\t<option value='MXN'>MXN</option>\n"
"\t\t\t<option value='CHF'>CHF</option>\n"
"\t\t\t<option value='HKD'>HKD</option>\n"
"\t\t\t<option value='HUF'>HUF</option>\n"
"\t\t\t<option value='ILS'>ILS</option>\n"
"\t\t\t<option value='BRL'>BRL</option>\n"
"\t\t\t<option value='JPY'>JPY</option>\n"
"\t\t\t<option value='MYR'>MYR</option>\n"
"\t\t\t<option value='PHP'>PHP</option>\n"
"\t\t\t<option value='SGD'>SGD</option>\n"
"\t\t\t<option value='TWD'>TWD</option>\n"
"\t\t\t<option value='THB'>THB</option>\n"
"\t</select>\n"
"\t<br/>\n"
"\t<input type=\"radio\" name=\"p3\" value=\"1\" id=\"sub_m\" checked=\"checked\" /><label for=\"sub_m\">monthly</label>\n"
"\t<input type=\"radio\" name=\"p3\" value=\"3\" id=\"sub_q\"/><label for=\"sub_q\">quarterly</label>\n"
"\t<input type=\"radio\" name=\"p3\" value=\"12\" id=\"sub_y\"/><label for=\"sub_y\">yearly</label>\n"
"\t<br/>\n"
"\t<input type=\"submit\" value=\"Subscribe\" class=\"button\" />\n"
"</form>\n"
msgstr ""
"<form action=\"https://www.paypal.com/cgi-bin/webscr\" method=\"post\" target='_blank' class='donation'>\n"
"\t<input type=\"hidden\" name=\"cmd\" value=\"_xclick-subscriptions\"/>\n"
"\t<input type=\"hidden\" name=\"business\" value=\"tails@torservers.net\"/>\n"
"\t<input type=\"hidden\" name=\"item_name\" value=\"Tails recurring donation\"/>\n"
"\t<input type=\"hidden\" name=\"no_note\" value=\"1\"/>\n"
"\t<input type=\"hidden\" name=\"src\" value=\"1\"/>\n"
"\t<input type=\"hidden\" name=\"modify\" value=\"1\"/>\n"
"\t<input type=\"hidden\" name=\"t3\" value=\"M\"/>\n"
"\t<input name=\"lc\" type=\"hidden\" value=\"US\" />\n"
"\t<input type=\"radio\" name=\"a3\" value=\"5\" id=\"sub5\" checked=\"checked\" /><label for=\"sub5\">5</label>\n"
"\t<input type=\"radio\" name=\"a3\" value=\"10\" id=\"sub10\"/><label for=\"sub10\">10</label>\n"
"\t<input type=\"radio\" name=\"a3\" value=\"20\" id=\"sub20\"/><label for=\"sub20\">20</label>\n"
"\t<input type=\"radio\" name=\"a3\" value=\"50\" id=\"sub50\"/><label for=\"sub50\">50</label>\n"
"\t<input type=\"radio\" name=\"a3\" value=\"100\" id=\"sub100\"/><label for=\"sub100\">100</label>\n"
"\t<input type=\"radio\" name=\"a3\" value=\"250\" id=\"sub250\"/><label for=\"sub250\">250</label>\n"
"\t<input type=\"radio\" name=\"a3\" value=\"500\" id=\"sub500\"/><label for=\"sub500\">500</label>\n"
"\t<select name=\"currency_code\">\n"
"\t\t\t<option value='EUR'>EUR</option>\n"
"\t\t\t<option value='USD'>USD</option>\n"
"\t\t\t<option value='GBP'>GBP</option>\n"
"\t\t\t<option value='CAD'>CAD</option>\n"
"\t\t\t<option value='AUD'>AUD</option>\n"
"\t\t\t<option value='NZD'>NZD</option>\n"
"\t\t\t<option value='SEK'>SEK</option>\n"
"\t\t\t<option value='CZK'>CZK</option>\n"
"\t\t\t<option value='PLN'>PLN</option>\n"
"\t\t\t<option value='DKK'>DKK</option>\n"
"\t\t\t<option value='NOK'>NOK</option>\n"
"\t\t\t<option value='MXN'>MXN</option>\n"
"\t\t\t<option value='CHF'>CHF</option>\n"
"\t\t\t<option value='HKD'>HKD</option>\n"
"\t\t\t<option value='HUF'>HUF</option>\n"
"\t\t\t<option value='ILS'>ILS</option>\n"
"\t\t\t<option value='BRL'>BRL</option>\n"
"\t\t\t<option value='JPY'>JPY</option>\n"
"\t\t\t<option value='MYR'>MYR</option>\n"
"\t\t\t<option value='PHP'>PHP</option>\n"
"\t\t\t<option value='SGD'>SGD</option>\n"
"\t\t\t<option value='TWD'>TWD</option>\n"
"\t\t\t<option value='THB'>THB</option>\n"
"\t</select>\n"
"\t<br/>\n"
"\t<input type=\"radio\" name=\"p3\" value=\"1\" id=\"sub_m\" checked=\"checked\" /><label for=\"sub_m\">monatlich</label>\n"
"\t<input type=\"radio\" name=\"p3\" value=\"3\" id=\"sub_q\"/><label for=\"sub_q\">quartalsweise</label>\n"
"\t<input type=\"radio\" name=\"p3\" value=\"12\" id=\"sub_y\"/><label for=\"sub_y\">jährlich</label>\n"
"\t<br/>\n"
"\t<input type=\"submit\" value=\"Absenden\" class=\"button\" />\n"
"</form>\n"

#. type: Title ###
#, no-wrap
msgid "Make a one-time donation"
msgstr "Eine einmalige Spende tätigen"

#. type: Plain text
#, no-wrap
msgid ""
"<form action='https://www.paypal.com/cgi-bin/webscr' id='paypalForm' method='post' target='_blank' class='donation'>\n"
"\t<input name='cmd' type='hidden' value='_donations' />\n"
"\t<input name='business' type='hidden' value='tails@torservers.net' />\n"
"\t<input name='item_name' type='hidden' value='Tails one-time donation' />\n"
"\t<input type=\"hidden\" name=\"no_shipping\" value=\"1\"/>\n"
"\t<input name=\"lc\" type=\"hidden\" value=\"US\" />\n"
"\t<input type=\"radio\" name=\"amount\" value=\"5\" id=\"pp_5\" /><label for=\"pp_5\">5</label>\n"
"\t<input type=\"radio\" name=\"amount\" value=\"10\" id=\"pp_10\"/><label for=\"pp_10\">10</label>\n"
"\t<input type=\"radio\" name=\"amount\" value=\"20\" id=\"pp_20\"/><label for=\"pp_20\">20</label>\n"
"\t<input type=\"radio\" name=\"amount\" value=\"50\" id=\"pp_50\"/><label for=\"pp_50\">50</label>\n"
"\t<input type=\"radio\" name=\"amount\" value=\"100\" id=\"pp_100\"/><label for=\"pp_100\">100</label>\n"
"\t<input type=\"radio\" name=\"amount\" value=\"\" id=\"pp_cust\" checked=\"checked\"/><label for=\"pp_cust\">custom amount</label>\n"
"\t<select name=\"currency_code\">\n"
"\t\t<option value='EUR'>EUR</option>\n"
"\t\t<option value='USD'>USD</option>\n"
"\t\t<option value='GBP'>GBP</option>\n"
"\t\t<option value='CAD'>CAD</option>\n"
"\t\t<option value='AUD'>AUD</option>\n"
"\t\t<option value='NZD'>NZD</option>\n"
"\t\t<option value='SEK'>SEK</option>\n"
"\t\t<option value='CZK'>CZK</option>\n"
"\t\t<option value='PLN'>PLN</option>\n"
"\t\t<option value='DKK'>DKK</option>\n"
"\t\t<option value='NOK'>NOK</option>\n"
"\t\t<option value='MXN'>MXN</option>\n"
"\t\t<option value='CHF'>CHF</option>\n"
"\t\t<option value='HKD'>HKD</option>\n"
"\t\t<option value='HUF'>HUF</option>\n"
"\t\t<option value='ILS'>ILS</option>\n"
"\t\t<option value='BRL'>BRL</option>\n"
"\t\t<option value='JPY'>JPY</option>\n"
"\t\t<option value='MYR'>MYR</option>\n"
"\t\t<option value='PHP'>PHP</option>\n"
"\t\t<option value='SGD'>SGD</option>\n"
"\t\t<option value='TWD'>TWD</option>\n"
"\t\t<option value='THB'>THB</option>\n"
"\t</select>\n"
"\t<br/>\n"
"\t<input type=\"submit\" value=\"Donate\" class=\"button\" />\n"
"</form>\n"
msgstr ""
"<form action='https://www.paypal.com/cgi-bin/webscr' id='paypalForm' method='post' target='_blank' class='donation'>\n"
"\t<input name='cmd' type='hidden' value='_donations' />\n"
"\t<input name='business' type='hidden' value='tails@torservers.net' />\n"
"\t<input name='item_name' type='hidden' value='Tails one-time donation' />\n"
"\t<input type=\"hidden\" name=\"no_shipping\" value=\"1\"/>\n"
"\t<input name=\"lc\" type=\"hidden\" value=\"US\" />\n"
"\t<input type=\"radio\" name=\"amount\" value=\"5\" id=\"pp_5\" /><label for=\"pp_5\">5</label>\n"
"\t<input type=\"radio\" name=\"amount\" value=\"10\" id=\"pp_10\"/><label for=\"pp_10\">10</label>\n"
"\t<input type=\"radio\" name=\"amount\" value=\"20\" id=\"pp_20\"/><label for=\"pp_20\">20</label>\n"
"\t<input type=\"radio\" name=\"amount\" value=\"50\" id=\"pp_50\"/><label for=\"pp_50\">50</label>\n"
"\t<input type=\"radio\" name=\"amount\" value=\"100\" id=\"pp_100\"/><label for=\"pp_100\">100</label>\n"
"\t<input type=\"radio\" name=\"amount\" value=\"\" id=\"pp_cust\" checked=\"checked\"/><label for=\"pp_cust\">anderer Betrag</label>\n"
"\t<select name=\"currency_code\">\n"
"\t\t<option value='EUR'>EUR</option>\n"
"\t\t<option value='USD'>USD</option>\n"
"\t\t<option value='GBP'>GBP</option>\n"
"\t\t<option value='CAD'>CAD</option>\n"
"\t\t<option value='AUD'>AUD</option>\n"
"\t\t<option value='NZD'>NZD</option>\n"
"\t\t<option value='SEK'>SEK</option>\n"
"\t\t<option value='CZK'>CZK</option>\n"
"\t\t<option value='PLN'>PLN</option>\n"
"\t\t<option value='DKK'>DKK</option>\n"
"\t\t<option value='NOK'>NOK</option>\n"
"\t\t<option value='MXN'>MXN</option>\n"
"\t\t<option value='CHF'>CHF</option>\n"
"\t\t<option value='HKD'>HKD</option>\n"
"\t\t<option value='HUF'>HUF</option>\n"
"\t\t<option value='ILS'>ILS</option>\n"
"\t\t<option value='BRL'>BRL</option>\n"
"\t\t<option value='JPY'>JPY</option>\n"
"\t\t<option value='MYR'>MYR</option>\n"
"\t\t<option value='PHP'>PHP</option>\n"
"\t\t<option value='SGD'>SGD</option>\n"
"\t\t<option value='TWD'>TWD</option>\n"
"\t\t<option value='THB'>THB</option>\n"
"\t</select>\n"
"\t<br/>\n"
"\t<input type=\"submit\" value=\"Spenden\" class=\"button\" />\n"
"</form>\n"

#. type: Title =
#, no-wrap
msgid "How does Tails use this money?\n"
msgstr "Wofür verwendet Tails dieses Geld?\n"

#. type: Plain text
msgid ""
"Our [[financial documents|doc/about/finances]] are available for your review."
msgstr ""
"Unsere [[Auflistungen über die Verwendung der Zuwendungen|doc/about/"
"finances]] sind für Sie einsehbar."

#~ msgid ""
#~ "**Your support is critical to our success.** Consider making\n"
#~ "a donation to Tails.\n"
#~ msgstr ""
#~ "**Ihre Hilfe ist ausschlaggebend für unseren Erfolg.** Bitte erwägen "
#~ "Sie,\n"
#~ " Tails eine Spende zukommen zu lassen.\n"
