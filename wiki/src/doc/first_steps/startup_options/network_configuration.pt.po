# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2015-10-26 23:57+0100\n"
"PO-Revision-Date: 2014-06-21 19:18-0300\n"
"Last-Translator: Tails Developers <amnesia@boum.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Network configuration\"]]\n"
msgstr "[[!meta title=\"Configuração de rede\"]]\n"

#. type: Plain text
msgid ""
"Depending on your Internet connection, you might need to configure the way "
"Tor connects to the Internet. For example:"
msgstr ""
"Dependendo de sua conexão à Internet, pode ser que você precise configurar a "
"forma como o Tor se conecta à Internet. Por exemplo:"

#. type: Bullet: '- '
msgid "If you need to use a proxy to access the Internet."
msgstr "Se você usa um proxy para acessar a Intenet."

#. type: Bullet: '- '
msgid ""
"If your Internet connection goes through a firewall that only allows "
"connections to certain ports."
msgstr ""
"Se sua conexão à Internet passa através de um firewall que somente permite "
"conexões a determinadas portas."

#. type: Bullet: '- '
msgid ""
"If you want to use Tor bridges because your Internet connection is censored "
"or you want to hide the fact that you are using Tor. See also our "
"documentation on [[what are bridges and when to use them|bridge_mode]]."
msgstr ""
"Se você quer usar pontes Tor (Tor bridges) porque sua conexão à Internet é "
"censurada, ou se você quer esconder o fato de que você está usando Tor. Veja "
"também nossa documentação sobre [[o que são pontes e quando usá-las|"
"bridge_mode]]."

#. type: Plain text
#, no-wrap
msgid ""
"In these cases, choose to configure bridge, firewall, or proxy settings from\n"
"[[<span class=\"application\">Tails\n"
"Greeter</span>|startup_options#tails_greeter]]:\n"
msgstr ""
"Nestes casos, escolha para configurar as configurações de bridge, firewall\n"
"ou proxy a partir do [[<span class=\"application\">Tails\n"
"Greeter</span>|startup_options#tails_greeter]]:\n"

#. type: Bullet: '1. '
msgid ""
"When <span class=\"application\">Tails Greeter</span> appears, in the <span "
"class=\"guilabel\">Welcome to Tails</span> window, click on the <span class="
"\"button\">Yes</span> button. Then click on the <span class=\"button"
"\">Forward</span> button."
msgstr ""
"Quando o <span class=\"application\"> aparecer, na tela de <span class="
"\"guilabel\">Bem vindo/a ao Tails</span>, clique no botão <span class="
"\"button\">Sim</span>. A seguir, clique no botão <span class=\"button"
"\">Próximo</span>."

#. type: Bullet: '2. '
msgid ""
"In the <span class=\"guilabel\">Network configuration</span> section, select "
"the following option: <span class=\"guilabel\">This computer's Internet "
"connection is censored, filtered, or proxied.</span>"
msgstr ""
"Na seção de <span class=\"guilabel\">Configuração de rede</span>, selecione "
"a seguinte opção: <span class=\"guilabel\">A conexão à Internet deste "
"computador é censurada, filtrada ou passa através de um proxy.</span>"

#. type: Plain text
#, fuzzy
#| msgid ""
#| "Then, after starting the working session and connecting to the Internet, "
#| "an assistant will guide you through the configuration of Tor."
msgid ""
"Then, after starting the working session and connecting to the network, an "
"assistant will guide you through the configuration of Tor."
msgstr ""
"Então, depois de iniciar a sessão de trabalho e conectar à Internet, um "
"assistente vai te guiar através da configuração do Tor."

#. type: Plain text
#, no-wrap
msgid "<div class=\"tip\">\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<p>If, on the contrary, you want to work completely offline, you can choose to\n"
"[[disable all networking|offline_mode]].</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr ""
