# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2015-07-28 09:53+0200\n"
"PO-Revision-Date: 2015-10-19 06:45+0000\n"
"Last-Translator: sprint5 <translation5@451f.org>\n"
"Language-Team: Persian <http://weblate.451f.org:8889/projects/tails/numerous_"
"security_holes_in_023/fa/>\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 2.4-dev\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta date=\"Sun Apr 27 00:00:00 2014\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Numerous security holes in Tails 0.23\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!tag security/fixed]]\n"
msgstr "[[!tag security/fixed]]\n"

#. type: Plain text
msgid "Several security holes affect Tails 0.23."
msgstr ""

#. type: Plain text
msgid ""
"We **strongly** encourage you to [[upgrade to Tails 1.0|news/version_1.0]] "
"as soon as possible."
msgstr ""

#. type: Title =
#, no-wrap
msgid "Details\n"
msgstr "جزییات\n"

#. type: Bullet: ' - '
msgid ""
"Iceweasel and its bundled NSS: [[!mfsa2014 34]], [[!mfsa2014 35]], "
"[[!mfsa2014 37]], [[!mfsa2014 38]], [[!mfsa2014 42]], [[!mfsa2014 43]], "
"[[!mfsa2014 44]],"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   [[!mfsa2014 46]]\n"
msgstr ""

#. type: Bullet: ' - '
msgid "tor: [[!tor_bug 11464]]"
msgstr ""

#. type: Bullet: ' - '
msgid "openjdk-6: [[!debsa2014 2912]]"
msgstr ""

#. type: Bullet: ' - '
msgid "openssl: [[!debsa2014 2908]]"
msgstr ""

#. type: Bullet: ' - '
msgid "curl: [[!debsa2014 2902]]"
msgstr ""

#. type: Bullet: ' - '
msgid "openssh: [[!debsa2014 2894]]"
msgstr ""

#. type: Bullet: ' - '
msgid "libyaml: [[!debsa2014 2884]]"
msgstr ""

#. type: Bullet: ' - '
msgid "libyaml-libyaml-perl: [[!debsa2014 2885]]"
msgstr ""
