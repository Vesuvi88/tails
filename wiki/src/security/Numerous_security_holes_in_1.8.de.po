# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Tails\n"
"POT-Creation-Date: 2015-12-19 20:06+0100\n"
"PO-Revision-Date: 2015-12-22 00:29-0000\n"
"Last-Translator: Tails translators <amnesia@boum.org>\n"
"Language-Team: Tails translators <tails@boum.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.4\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta date=\"Thu Dec 17 13:24:15 CET 2015\"]]\n"
msgstr "[[!meta date=\"Thu Dec 17 13:24:15 CET 2015\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Numerous security holes in Tails 1.8\"]]\n"
msgstr "[[!meta title=\"Zahlreiche Sicherheitslücken in Tails 1.8\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!tag security/fixed]]\n"
msgstr "[[!tag security/fixed]]\n"

#. type: Plain text
msgid ""
"Several security holes that affect Tails 1.8 are now fixed in Tails 1.8.1."
msgstr ""
"Einige Sicherheitslücken, die Tails 1.8 betreffen, wurden in Tails 1.8.1 "
"behoben."

#. type: Plain text
msgid ""
"We **strongly** encourage you to [[upgrade to Tails 1.8.1|news/"
"version_1.8.1]] as soon as possible."
msgstr ""
"Wir empfehlen **dringend**, so schnell wie möglich auf [[Tails 1.8.1 zu "
"aktualisieren|news/version_1.8.1]]."

#. type: Title =
#, no-wrap
msgid "Details\n"
msgstr "Details\n"

#. type: Bullet: '* '
msgid "Tor Browser: [[!mfsa2015 134]], [[!mfsa2015 149]]"
msgstr "Tor Browser: [[!mfsa2015 134]], [[!mfsa2015 149]]"

#. type: Bullet: '* '
msgid "linux: [[!debsa2015 3426]]"
msgstr "linux: [[!debsa2015 3426]]"

#. type: Bullet: '* '
msgid "bind9: [[!debsa2015 3420]]"
msgstr "bind9: [[!debsa2015 3420]]"

#. type: Bullet: '* '
msgid "grub2: [[!debsa2015 3421]]"
msgstr "grub2: [[!debsa2015 3421]]"

#. type: Bullet: '* '
msgid "gdkpixbuf [[!debsa2015 3337]]"
msgstr "gdkpixbuf [[!debsa2015 3337]]"
