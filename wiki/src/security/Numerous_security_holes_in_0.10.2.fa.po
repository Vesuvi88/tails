# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2015-07-28 09:53+0200\n"
"PO-Revision-Date: 2015-10-19 06:45+0000\n"
"Last-Translator: sprint5 <translation5@451f.org>\n"
"Language-Team: Persian <http://weblate.451f.org:8889/projects/tails/numerous_"
"security_holes_in_0102/fa/>\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 2.4-dev\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta date=\"Sun Apr 22 00:00:00 2012\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Numerous security holes in Tails 0.10.2\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!tag security/fixed]]\n"
msgstr "[[!tag security/fixed]]\n"

#. type: Plain text
msgid "The following security holes affect Tails 0.10.2."
msgstr ""

#. type: Plain text
msgid ""
"We **strongly** urge you to [[upgrade to Tails 0.11|news/version_0.11]] as "
"soon as possible in case you are still using an older version."
msgstr ""

#. type: Title =
#, no-wrap
msgid "Details\n"
msgstr "جزییات\n"

#. type: Bullet: '  - '
msgid "iceweasel ([[!debsa2012 2457]], [[!debsa2012 2433]])"
msgstr ""

#. type: Bullet: '  - '
msgid "openssl ([[!debsa2012 2454]])"
msgstr ""

#. type: Bullet: '  - '
msgid "gimp ([[!debsa2012 2426]])"
msgstr ""

#. type: Bullet: '  - '
msgid "imagemagick ([[!debsa2012 2427]])"
msgstr ""

#. type: Bullet: '  - '
msgid "freetype ([[!debsa2012 2428]])"
msgstr ""

#. type: Bullet: '  - '
msgid "libpng ([[!debsa2012 2439]], [[!debsa2012 2446]])"
msgstr ""

#. type: Bullet: '  - '
msgid "libtasn1-3 ([[!debsa2012 2440]])"
msgstr ""

#. type: Bullet: '  - '
msgid "gnutls26 ([[!debsa2012 2441]])"
msgstr ""

#. type: Bullet: '  - '
msgid "tiff ([[!debsa2012 2447]])"
msgstr ""
