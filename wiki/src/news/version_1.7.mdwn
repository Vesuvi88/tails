[[!meta date="Tue Nov 03 12:34:56 2015"]]
[[!meta title="Tails 1.7 is out"]]
[[!tag announce]]

This release fixes [[numerous security
issues|security/Numerous_security_holes_in_1.6]]. All users must
upgrade as soon as possible.

[[!toc levels=1]]

# Changes

[[!inline pages="inc/release_notes/1.7" raw="yes"]]

# Known issues

See the current list of [[known issues|support/known_issues]].

# Download or upgrade

Go to the [[download]] or [[upgrade|doc/first_steps/upgrade/]] page.

If you have been updating automatically for a while and your Tails does not
boot after an automatic upgrade, you can [[update your Tails
manually|doc/first_steps/upgrade#manual]].

# What's coming up?

The next Tails release is [[scheduled|contribute/calendar]] for
December 15.

Have a look at our [[!tails_roadmap]] to see where we are heading to.

We need your help and there are many ways to [[contribute to
Tails|contribute]] ([[donating|contribute/how/donate]] is only one of
them). Come [[talk to us|contribute/talk]]!
