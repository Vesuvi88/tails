# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2014-02-07 13:34+0100\n"
"PO-Revision-Date: 2014-02-07 21:15+0100\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta date=\"Wed Feb 5 10:00:00 2014\"]]\n"
msgstr "[[!meta date=\"Wed Feb 5 10:00:00 2014\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Tails 0.22.1 is out\"]]\n"
msgstr "[[!meta title=\"Tails 0.22.1 ist da.\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!tag announce]]\n"
msgstr ""

#. type: Plain text
msgid "Tails, The Amnesic Incognito Live System, version 0.22.1, is out."
msgstr "Tails, The Amnesic Incognito Live System, version 0.22.1, ist da."

#. type: Plain text
msgid ""
"All users must upgrade as soon as possible: this release fixes [[numerous "
"security issues|security/Numerous_security_holes_in_0.22]]."
msgstr ""
"Alle Benutzer sollten schnellstmöglich umsteigen: dieser Release behebt "
"[[verschiedene Sicherheitslücken|security/Numerous_security_holes_in_0.22]]."

#. type: Plain text
#, no-wrap
msgid "[[!toc levels=1]]\n"
msgstr ""

#. type: Title #
#, no-wrap
msgid "Changes"
msgstr "Änderungen"

#. type: Plain text
msgid "Notable user-visible changes include:"
msgstr "Relevante für Benutzer sichtbare Änderungen beinhalten:"

#. type: Plain text
#, no-wrap
msgid ""
"* Security fixes\n"
"  - Upgrade the web browser to 24.3.0esr, that fixes a few serious\n"
"    security issues.\n"
"  - Upgrade the system NSS to 3.14.5, that fixes a few serious\n"
"    security issues.\n"
"  - Workaround a browser size fingerprinting issue by using small\n"
"    icons in the web browser's navigation toolbar.\n"
"  - Upgrade Pidgin to 2.10.8, that fixes a number of serious\n"
"    security issues.\n"
msgstr ""
"* Behobene Sicherheitslücken\n"
"  - Aktualisierung von Iceweasel auf die Version 24.3.0esr,\n"
"    die einige ernsthafte Sicherheitslücken schließt.\n"
"  - Aktualisierung der NSS auf die Version 3.14.5, die einige\n"
"    ernsthafte Sicherheitslücken schließt.\n"
"  - Umgehung eines Problems der Erkennung der Browsergröße aus der\n"
"    Ferne durch Verwendung kleiner Icons in der Navigationsleiste\n"
"    des Webbrowsers.\n"
"  - Aktualisierung von Pidgin auf 2.10.8, die einige schwere\n"
"    Sicherheitslücken behebt.\n"

#. type: Plain text
#, no-wrap
msgid ""
"* Major improvements\n"
"  - Check for upgrades availability using Tails Upgrader, and propose\n"
"    to apply an incremental upgrade whenever possible.\n"
"  - Install Linux 3.12 (3.12.6-2).\n"
msgstr ""
"* Wichtige Verbesserungen\n"
"  - Überprüfung der Verfügbarkeit von Upgrades durch den Tails Upgrader\n"
"    und Vorschlag von inkrementellen Upgrades, falls dies möglich\n"
"    ist.\n"
"  - Installation von Linux 3.12 (3.12.6-2).\n"

#. type: Plain text
#, no-wrap
msgid ""
"* Bugfixes\n"
"  - Fix the keybindings problem introduced in 0.22.\n"
"  - Fix the Unsafe Browser problem introduced in 0.22.\n"
"  - Use IE's icon in Windows camouflage mode.\n"
"  - Handle some corner cases better in Tails Installer.\n"
"  - Use the correct browser homepage in Spanish locales.\n"
msgstr ""
"* Behobene Fehler\n"
"  - Fix der in 0.22 eingeführten Probleme mit Tastenbelegungen.\n"
"  - Fix der in 0.22 eingeführten Probleme mit dem Unsafe Browser.\n"
"  - Nutzung des IE-Icons im Windows-Carmouflage-Modus.\n"
"  - Verbesserung der Behandlung einiger Ausnahmefälle im Tails\n"
"    Installer.\n"
"  - Nutzung der korrekten Browser-Homepage in spanischen Spracheinstellungen.\n"

#. type: Plain text
#, no-wrap
msgid ""
"* Minor improvements\n"
"  - Update Torbutton to 1.6.5.3.\n"
"  - Do not start Tor Browser automatically, but notify when Tor\n"
"    is ready.\n"
"  - Import latest Tor Browser prefs.\n"
"  - Many user interface improvements in Tails Upgrader.\n"
msgstr ""
"* Kleinere Verbesserungen\n"
"  - Aktualisierung von Torbutton auf die Version 1.6.5.3.\n"
"  - Starte den Tor Browser nicht automatisch aber melde,\n"
"    wenn Tor bereit ist.\n"
"  - Import der neuesten Tor Browser Präferenzen.\n"
"  - Verschiedene Verbesserungen des User-Interfaces des Tails Upgraders.\n"

#. type: Plain text
msgid ""
"See the [online Changelog](https://git-tails.immerda.ch/tails/plain/debian/"
"changelog)  for technical details."
msgstr "Technische Details finden Sie im [Changelog](https://git-tails.immerda.ch/tails/plain/debian/changelog)."

#. type: Title #
#, no-wrap
msgid "Known issues"
msgstr "Bekannte Probleme"

#. type: Bullet: '* '
msgid ""
"The memory erasure on shutdown [[!tails_ticket 6460 desc=\"does not work on "
"some hardware\"]]."
msgstr ""
"Die Löschung des Arbeitsspeichers beim Herunterfahren [[!tails_ticket 6460 "
"desc=\"funktioniert auf einigen Systemen nicht\"]]."

#. type: Bullet: '* '
msgid "[[Longstanding|support/known_issues]] known issues."
msgstr "[[Schon länger bekannte|support/known_issues]] Probleme."

#. type: Title #
#, no-wrap
msgid "I want to try it or to upgrade!"
msgstr "Ich möchte es ausprobieren oder upgraden!"

#. type: Plain text
msgid "Go to the [[download]] page."
msgstr "Gehen Sie zu der [[Download]]-Seite."

#. type: Plain text
msgid ""
"As no software is ever perfect, we maintain a list of [[problems that "
"affects the last release of Tails|support/known_issues]]."
msgstr ""
"Da keine Software jemals perfekt ist, führen wir eine Liste von [[Problemen, "
"die den letzten Release von Tail betreffen|support/known_issues]]."

#. type: Title #
#, no-wrap
msgid "What's coming up?"
msgstr "Was kommt als nächstes?"

#. type: Plain text
msgid ""
"The next Tails release is [[scheduled|contribute/calendar]] for March 18."
msgstr ""
"Der nächste Tails-Release ist für den 18. März [[geplant|contribute/"
"calendar]]."

#. type: Plain text
msgid "Have a look to our [[!tails_roadmap]] to see where we are heading to."
msgstr "Werfen Sie einen Blick auf unsere [[!tails_roadmap desc=\"Roadmap\"]], um zu sehen worauf wir zusteuern."

#. type: Plain text
msgid ""
"Would you want to help? There are many ways [[**you** can contribute to "
"Tails|contribute]]. If you want to help, come talk to us!"
msgstr ""
"Möchten Sie uns helfen? Es gibt viele Wege auf denen [[**Sie** zu Tails "
"beitragen können|contribute]]. Falls Sie helfen wollen, dann sprechen Sie "
"mit uns!"
